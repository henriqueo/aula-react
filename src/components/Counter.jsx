import {useState } from 'react'

export function Counter() {
    const [counter, setCounter] = useState(0) // Todas as variáveis do React são imutáveis, setCounter cria uma nova

    function increment(){
        setCounter(counter + 1)
    }

    return (
        <div>
            <h2>{counter}</h2>
            <button onClick={increment}>Incrementar</button>
        </div>
    )
}
